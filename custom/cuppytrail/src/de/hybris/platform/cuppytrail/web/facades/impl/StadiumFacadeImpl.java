package de.hybris.platform.cuppytrail.web.facades.impl;

import de.hybris.platform.cuppytrail.data.StadiumData;
import de.hybris.platform.cuppytrail.data.StadiumDataTO;
import de.hybris.platform.cuppytrail.facades.StadiumFacade;
import de.hybris.platform.cuppytrail.stadiumService.StadiumService;

import java.util.ArrayList;
import java.util.List;


public class StadiumFacadeImpl implements StadiumFacade {

    private StadiumService stadiumService;

    @Override
    public StadiumDataTO getStadiumDetails(String name) {
       StadiumDataTO stadiumDataTO= new StadiumDataTO();
        stadiumDataTO.setName("Wembley");
        stadiumDataTO.setCapacity("12345");
       return stadiumDataTO;
    }

    @Override
    public StadiumService getStadiumService() {
        return stadiumService;
    }

    public void setStadiumService(final StadiumService stadiumService){
        this.stadiumService = stadiumService;
    }

    @Override
    public StadiumData getStadium(String name) {
        return null;
    }

    @Override
    public List<StadiumData> getStadiums() {
        return null;
    }

    @Override
    public List<StadiumDataTO> getAllStadium() {
        final List<StadiumDataTO> dataTOs = new ArrayList<StadiumDataTO>();
        StadiumDataTO stadiumDataTO = new StadiumDataTO();
        stadiumDataTO.setName("Wembley");
        stadiumDataTO.setCapacity("12345");
        dataTOs.add(stadiumDataTO);
        return dataTOs;
    }
}
