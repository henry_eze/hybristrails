package de.hybris.platform.cuppytrail.facades;

import de.hybris.platform.cuppytrail.data.StadiumData;
import de.hybris.platform.cuppytrail.data.StadiumDataTO;
import de.hybris.platform.cuppytrail.model.StadiumModel;
import de.hybris.platform.cuppytrail.stadiumService.StadiumService;

import java.util.List;

public interface StadiumFacade {

    StadiumData getStadium(String name);

    List<StadiumData> getStadiums();

    List<StadiumDataTO> getAllStadium();

    StadiumDataTO getStadiumDetails(String code);

    StadiumService getStadiumService();

    void setStadiumService(StadiumService stadiumService);
}
