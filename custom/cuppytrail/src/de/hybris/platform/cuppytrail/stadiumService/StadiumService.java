package de.hybris.platform.cuppytrail.stadiumService;

import de.hybris.platform.cuppytrail.model.StadiumModel;

import java.util.List;

public interface StadiumService {
    StadiumModel getStadiumForCode(String code);

    List<StadiumModel> getStadiums();

    List<StadiumModel> getAllStadium();

    StadiumModel getStadiumDetails(String wembly);
}
